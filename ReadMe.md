#### Программа                                                                                                                       "task-manager" ver 1.0.3
##### Требования к Software
###### Open JDK 11
##### Стек Технологий  
###### Apache Maven 3.6.1
##### Разработчик
###### Налимов Сергей Викторович. email - nalimov_sv@nlmk.com
##### Сборка приложения  
###### mvn clear / mvn install
##### Запуск приложения
```
jse-03>java -jar ./target/task-manager-1.0.2.jar help
```
##### Ключи запуска приложения
```
version - Display program version.
about - Display developer info.
help - Display list of terminal commands.
exit - Exit

project-create - Create new project by name
project-list - Display list of projects
project-clear - Remove all project
project-view-by-index - display project by index
project-view-by-id - display project by id
project-remove-by-name - delete project by name
project-remove-by-id - delete project by id
project-remove-by-index - delete project by index
project-update-by-index - update project by index
project-update-by-id - update project by id

task-create - Create new task by name
task-list - Display list of tasks
task-clear - Remove all task
task-view-by-index - display project by index
task-view-by-id - display project by id
task-remove-by-name - delete task by name
task-remove-by-id - delete task by id
task-remove-by-index - delete task by index
task-update-by-index - update task by index
task-update-by-id - update project by id

task-list-by-project-id - Display task list by project id
task-add-to-project-by-ids - Add task to project by ids
task-remove-from-project-by-ids - Remove task from project by ids
```
