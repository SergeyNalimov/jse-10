package ru.nalimov.tm.controller;

public class SystemController {

    public int displayVersion() {
        System.out.println("ver 1.0.3");
        return 0;
    }

    public int displayAbout() {
        System.out.println("nalimov_sv@nlmk.com");
        return 0;
    }

    public int displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println();
        System.out.println("project-create - Create new project by name");
        System.out.println("project-list - Display list of projects");
        System.out.println("project-clear - Remove all project");
        System.out.println("project-view-by-index - display project by index");
        System.out.println("project-view-by-id - display project by id");
        System.out.println("project-remove-by-name - delete project by name");
        System.out.println("project-remove-by-id - delete project by id");
        System.out.println("project-remove-by-index - delete project by index");
        System.out.println("project-update-by-index - update project by index");
        System.out.println("project-update-by-id - update project by id");
        System.out.println();
        System.out.println("task-create - create new task by name");
        System.out.println("task-list - display list of tasks");
        System.out.println("task-clear - remove all task");
        System.out.println("task-view-by-index - display project by index");
        System.out.println("task-view-by-id - display project by id");
        System.out.println("task-remove-by-name - delete task by name");
        System.out.println("task-remove-by-id - delete task by id");
        System.out.println("task-remove-by-index - delete task by index");
        System.out.println("task-update-by-index - update task by index");
        System.out.println("task-update-by-id - update project by id");
        System.out.println();
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids");


        return 0;
    }

    public int displayExit() {
        System.out.println("Exit...");
        return 0;
    }

    public void displayWelcome() {
        System.out.println("Welcome");
    }

    public int displayErr() {
        System.out.println("Error! Unknow key...");
        return -1;
    }
}
